Settings
--------

There are two types of settings available in Camelot:

# global settings, which affect all surveys, and
# survey settings, which are specific to a single survey

Global settings
~~~~~~~~~~~~~~~~~~
In the top right hand corner of the main navigation is the toggle to show the settings menu. The settings menu in Camelot provides a couple of global options:

.. figure:: screenshot/settings.png
   :alt: 

- **Species name style**: Whether species scientific or common names should be shown by default. Default is scientific names.
- **Send anonymous usage data**: Enabled this option will provide data about how you use Camelot back to the Camelot Project. This data is anonymous and is used only for the purpose of improving Camelot. It is disabled by default, though if you have the consent of others using your Camelot instance, please consider enabling this.

Survey details
~~~~~~~~~~~~~~
A survey can be configured via:

**Settings** → **Survey details**

Apart from the name and description, there is the option to configure the *sighting independence threshold*.  The sighting independence threshold is the number of minutes between two sightings where they may be classified *dependent*. Dependent sightings will be counted as a single sighting in some reports.

For more information about how the sighting independence threshold affects reports, see |indep_obs|.

.. |indep_obs| raw:: html

   <a href="reports.html#independent-observations">Independent observations</a>

